Source: nebula
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Alex David <flu0r1ne@flu0r1ne.net>
Section: golang
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-skip2-go-qrcode-dev,
               golang-github-anmitsu-go-shlex-dev,
               golang-github-armon-go-radix-dev,
               golang-github-cyberdelia-go-metrics-graphite-dev,
               golang-gvisor-gvisor-dev,
               golang-github-imdario-mergo-dev,
               golang-github-kardianos-service-dev,
               golang-github-miekg-dns-dev,
               golang-github-stretchr-testify-dev,
               golang-github-vishvananda-netlink-dev,
               golang-github-nbrownus-go-metrics-prometheus-dev,
               golang-github-gogo-protobuf-dev,
               golang-github-flynn-noise-dev,
               golang-golang-x-crypto-dev,
               golang-golang-x-net-dev,
               golang-golang-x-sys-dev,
               golang-google-protobuf-dev,
               golang-logrus-dev,
               golang-github-rcrowley-go-metrics-dev,
               golang-github-prometheus-client-golang-dev,
               golang-yaml.v2-dev
Standards-Version: 4.6.1.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/nebula
Vcs-Git: https://salsa.debian.org/go-team/packages/nebula.git
Homepage: https://github.com/slackhq/nebula
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/slackhq/nebula

Package: nebula
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Built-Using: ${misc:Built-Using}
Description: performant, scalable network overlay
 Nebula is a scalable overlay networking tool with a focus on performance,
 simplicity and security. It is intended to be more robust than current virtual
 networking solutions by providing peer-to-peer connectivity and NAT traversal
 mechanisms. Nebula uses state-of-the-art cryptography based on the "Noise"
 protocol.
 .
 This package provide the binaries and default configuration for Nebula.

Package: golang-github-slackhq-nebula-dev
Architecture: all
Multi-Arch: foreign
Depends: golang-github-skip2-go-qrcode-dev,
         golang-github-anmitsu-go-shlex-dev,
         golang-github-armon-go-radix-dev,
         golang-github-cyberdelia-go-metrics-graphite-dev,
         golang-github-imdario-mergo-dev,
         golang-github-kardianos-service-dev,
         golang-github-miekg-dns-dev,
         golang-github-stretchr-testify-dev,
         golang-github-vishvananda-netlink-dev,
         golang-github-nbrownus-go-metrics-prometheus-dev,
         golang-gogoprotobuf-dev,
         golang-github-flynn-noise-dev,
         golang-golang-x-crypto-dev,
         golang-golang-x-net-dev,
         golang-golang-x-sys-dev,
         golang-google-protobuf-dev,
         golang-logrus-dev,
         golang-github-rcrowley-go-metrics-dev,
         golang-github-prometheus-client-golang-dev,
         golang-yaml.v2-dev,
         ${misc:Depends}
Description: performant, scalable network overlay (Go library)
 Nebula is a scalable overlay networking tool with a focus on performance,
 simplicity and security.
 .
 This package provides the source files as Go library.
